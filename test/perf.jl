# comparing performance with matrices and sparse matrices

println("Warming up @time macro:")
@time 1

using Ragged
const nc = 10^6
const nr = 10^2
col = randperm(nr)

mat = rand(Int,nr,nc)
lili = Vector{Int}[deepcopy(col) for i=1:nc]
ra = RaggedArray(lili)
sp = sparse(mat)
test = Dict()
test[:mat] = mat
test[:lili] = lili
test[:ra] = ra
test[:sp] = sp

println("\ni-j indexing with @inbounds")
function accumij(in)
    out = 0
    @inbounds for j=1:nc, i=1:nr
        out += in[i,j]
    end
    out
end
function accumij(in::Vector{Vector{Int}})
    out = 0
    @inbounds for j=1:nc, i=1:nr
        out += in[j][i]
    end
    out
end
for (k,v) in test
    accumij(v)
end
for (k,v) in test
    println(k)
    @time accumij(v)
end

println("\nj-i indexing")
function accumji(in)
    out = 0
    for i=1:nr, j=1:nc
        out += in[i,j]
    end
    out
end
function accumji(in::Vector{Vector{Int}})
    out = 0
    for i=1:nr, j=1:nc
        out += in[j][i]
    end
    out
end
for (k,v) in test
    accumji(v)
end
for (k,v) in test
    println(k)
    @time accumji(v)
end
