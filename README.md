This implements a ragged array datatype in Julia.

To install:
```
Pkg.clone("git@bitbucket.org:maurow/ragged.jl.git", "Ragged")
```

It can be used almoste like a normal array:
```
ra = RaggedArray(Vector{Int}[[2,4], [1,3,4], [2,4,5], [1,2,3], [3]])
ra[:,1] #==[2,4]
ra[3,2] #==4
ra[4,2] # bounds error
```
